# Untitled Framework

A small, first timer framework. No dependancies or libraries used, but not saying they cannot\will not be added.

## Directory Map
```
╔app
╠═controllers
╠═core
╠═models
╠╦views
║╚home
╚web
 ╚css
```
### App
App is the main directory of the application, this doesn't nessicarily need to be inside the document root, as we do use .htaccess to

### Controllers
Controllers is where your controllers go when creating them they should always extend controller when created like so
`class name extends Controller`

Calling models from controllers should be done using the name of the class only
`$this->model('User');` will attempt to load 'User.php' in the models folder.

### Core
Core is where the core application lives. This contains the Main controller, Application functions, such as routing. Not much should be needed to be done to this, unless your extending, or forking this framework itself.

### Models
Models is where all data manipulation and handling is done. Because this framework does not include any dependancies or libraries for data interaction, you'll have to build them yourself, such as `Class User Extends PDO` for a class that would handle users in a database, etc. (I may possibly implement something like Illuminate)

### Views
The views folder is where all your html/display logic should live. since the basis of MVC is to abstract out each part of the handling, viewing, and controlling of data. Subfolders are used purely for organization.

### Web
The Web folder and subfolders is where the publicly viewable files should live, or should be the DocRoot for security, Though .htaccess files can be used to restrict access to these directories such as 'app' or 'models' and while viewing them directly would have no purpose, for added security it would be best to just keep the files that only need to be public in this folder. Such as CSS, Javascript, etc.

#### Resources
This application was built following the Codecourse 'Build a PHP MVC Application' Playlist located [here.](https://www.youtube.com/playlist?list=PLfdtiltiRHWGXVHXX09fxXDi-DqInchFD)

I fully intend to continually develop this application for personal use on future projects, contributions and comments and happily accepted.